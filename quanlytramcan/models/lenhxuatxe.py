# -*- coding: utf-8 -*-

from odoo import models, fields


class LenhXuatXe(models.Model):
    _name = "tgl.lenhxuatxe"

    name = fields.Char(string="Tên lệnh xuất xe", required=True)
    id_xe = fields.Many2one("bms.epc", string="Xe", required=True)
    khoiluongxuat = fields.Float(string="Khối lượng xuất")
    thoigian = fields.Datetime(string="Thời gian xuất")
    nguoiduyet = fields.Many2one("hr.employee", string="Người duyệt", required=True)
    khachhang = fields.Many2one("res.users", string="Khách hàng", required=True)

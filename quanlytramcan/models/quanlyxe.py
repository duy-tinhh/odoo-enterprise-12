# -*- coding: utf-8 -*-

from odoo import models, fields, api
import requests, json

class DanhMucXe(models.Model):
    _name = 'bms.epc'

    name = fields.Char(string="Biển số xe", required=True)
    trongluongxe = fields.Float(string="Trọng lượng xe rỗng",  required=True)
    taixe_id = fields.Many2one("hr.employee", string="Chọn tài xế")
    tramcanxe_ids = fields.One2many("tgl.tramcanxe","id_xe",string="Các lần cân")

    @api.model
    def find_did_number(self, sodep):
        print("**************")
        print(sodep)
        print("**************")
        response = requests.get('http://178.128.112.190:8080/login?username=test&password=test')

        print(response)
        print(type(response))
        json_data = json.loads(response.content.decode('utf-8'))
        print(json_data)
        print(type(json_data))
        did_token = json_data['token']
        print(did_token)

        url_tim_kiem = 'http://178.128.112.190:8080/did?token='
        url_tim_kiem += str(did_token)
        url_tim_kiem += '&pattern='
        url_tim_kiem += str(sodep)
        print(url_tim_kiem)
        tim_so = requests.get(url_tim_kiem)
        json_data_so = json.loads(tim_so.content.decode('utf-8'))['data']
        print(json_data_so)
        
        return json_data_so

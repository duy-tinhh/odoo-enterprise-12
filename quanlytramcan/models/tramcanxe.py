# -*- coding: utf-8 -*-

from odoo import models, fields, api


class TramCanXe(models.Model):
    _name = "tgl.tramcanxe"

    thoigian = fields.Datetime(string="Thời Gian", required=True)
    vaora = fields.Boolean(string="Vào Ra")
    trongluong = fields.Float(string="Trọng Lượng Cân",  required=True)
    id_xe = fields.Many2one(comodel_name="bms.epc", string="Xe",
                            required=True, ondelete='cascade')
    thoigianstop = fields.Datetime(string="Thời Gian Xong", required=True)
    

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = "%s" % (record.thoigian)
            result.append((record.id, name))
        return result

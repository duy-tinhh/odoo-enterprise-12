// Check jQuery available
if (typeof jQuery === 'undefined') { throw new Error('Để tìm kiếm số bắt buộc phải bật javascript') }

+function ($) {
    'use strict';
    $(document).ready(function() {
        var button = $('.ccall_find');
        button.click(function() {
            console.log("Hello there");
             odoo.define('web.ajax', function (require) {
                "use strict";
                chuoi_tim_kiem = document.getElementById('chuoi_tim_kiem').value;
                var rpc = require('web.rpc');
                rpc.query({
                     model: 'bms.epc',
                     method: 'find_did_number',
                    args: [chuoi_tim_kiem],
                }).then(function(data){
                    $('#bang_so_dep').find('td').parents('tr').remove();
                    var $table = document.getElementById("bang_so_dep");
                    data.forEach(function(so_tim_duoc) {
                      var noidung = '<tr><td>'+so_tim_duoc.isdn+'</td>';
                      noidung = noidung + '<td>'+so_tim_duoc.golden_number_fee+'</td>';
                      noidung = noidung + '<td>'+so_tim_duoc.sub_fee+'</td>';
                      noidung = noidung + '<td>'+so_tim_duoc.telco_code+'</td><';
                      noidung = noidung + '<td>'+so_tim_duoc.status+'</td></tr>';
                      $('#bang_so_dep tr:last').after(noidung);
                    });
                });
             });

        });
    });

}(jQuery);
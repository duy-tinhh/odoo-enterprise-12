# -*- coding: utf-8 -*-

from odoo import http, _
from odoo.http import request
import json

class Quanlytramcan(http.Controller):

    @http.route('/danh-sach-xe/', auth='public', website=True)
    def index(self, **kw):
        Danhsachxe = http.request.env['bms.epc']
        return http.request.render('bms_epc.DanhSachXe', {
            'Danhsachxe': Danhsachxe.sudo().search([])
        })

    @http.route('/chi-tiet-xe/<int:id>/', auth='public', website=True)
    def indexId(self, id):
        Chitietxe = http.request.env['bms.epc']
        print("--------------------------------------")
        return http.request.render('bms_epc.ChiTietXeId', {
            'Chitietxe': Chitietxe.search([
                ('id', '=', id),
            ])
        })

    @http.route(['/crm/contactus'], auth="public", type="json", csrf=False)
    def epc_contactus(self, **kwargs):
        print("Hello Thach Pham")

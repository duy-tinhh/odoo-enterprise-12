# -*- coding: utf-8 -*-

from odoo import models, fields, api
import requests
import json
import random


class bmsEPC(models.Model):
    _name = 'bms.epc'

    name = fields.Char(string="Account", required=True)
    password = fields.Char(string="Password", required=True)
    api_server = fields.Char(string="API Server", required=True)
    golden_number_fee_id = fields.Many2one('product.attribute')
    setup_fee_id = fields.Many2one("product.attribute")
    sub_fee_id = fields.Many2one('product.attribute')
    Product_1900 = fields.Many2one("product.template")
    Product_1800 = fields.Many2one("product.template")
    number_cd = fields.Many2one("product.template", string="Đầu số cố định")
    number_dd = fields.Many2one("product.template", string="Đầu số di động")

    @api.model
    def find_did_number(self, phone_pattern):
        server = self.env.ref('bms_epc.bms_epc_record')
        login = server.api_server + '/api/login?username=' + \
            server.name + '&password=' + server.password
        response = requests.get(login)
        json_data = json.loads(response.content.decode('utf-8'))
        did_token = json_data["token"]
        url_tim_kiem = server.api_server + '/api/did?token='
        url_tim_kiem += str(did_token)
        url_tim_kiem += '&pattern='
        url_tim_kiem += str(phone_pattern)
        tim_so = requests.get(url_tim_kiem)
        length_data = json.loads(tim_so.content.decode('utf-8'))["length"]
        if length_data == 0:
            json_data_so = []
        else:
            json_data_so = json.loads(tim_so.content.decode('utf-8'))["data"]
        return json_data_so

    @api.model
    def find_product_price(self, product_id, quantity_id):
        def takeSecond(elem):
            return elem[1]

        print("checkcheckcheckcheckcheckcheckcheckcheck")
        pricelist_ids = self.env['product.template'].browse(
            product_id).item_ids
        print(pricelist_ids)
        if len(pricelist_ids) == 0:
            return self.env['product.template'].browse(product_id).list_price
        min_quantity_array = []
        for pricelist_id in pricelist_ids:
            min_quantity_array.append(
                (pricelist_id.fixed_price, pricelist_id.min_quantity))
        min_quantity_array.sort(key=takeSecond)
        print(min_quantity_array)
        index = 0
        while (index < len(min_quantity_array)):

            if (index == len(min_quantity_array) - 1):
                if min_quantity_array[index][1] <= quantity_id:
                    return min_quantity_array[index][0]
            if min_quantity_array[index][1] <= quantity_id < min_quantity_array[index + 1][1]:
                return min_quantity_array[index][0]
            index += 1
        return self.env['product.template'].browse(product_id).list_price

    @api.model
    def find_product_product_price(self, product_id, quantity_id):
        def takeSecond(elem):
            return elem[1]

        print("checkcheckcheckcheckcheckcheckcheckcheck")
        pricelist_ids = self.env['product.product'].browse(
            product_id).item_ids
        print(pricelist_ids)
        if len(pricelist_ids) == 0:
            return self.env['product.product'].browse(product_id).list_price
        min_quantity_array = []
        for pricelist_id in pricelist_ids:
            min_quantity_array.append(
                (pricelist_id.fixed_price, pricelist_id.min_quantity))
        min_quantity_array.sort(key=takeSecond)
        print(min_quantity_array)
        index = 0
        while (index < len(min_quantity_array)):
            if (index == len(min_quantity_array) - 1):
                if min_quantity_array[index][1] <= quantity_id:
                    print(min_quantity_array[index][0])
                    return min_quantity_array[index][0]
            if min_quantity_array[index][1] <= quantity_id < min_quantity_array[index + 1][1]:
                print(min_quantity_array[index][0])
                return min_quantity_array[index][0]
            index += 1
        return self.env['product.product'].browse(product_id).list_price

    def check_golden_number_fee(self, product_id, golden_number_fee):
        print("Thach check golden number fee")
        print(product_id)
        print(golden_number_fee)
        product = self.env['product.template'].browse(product_id)
        print("check_golden_number_fee")
        golden_attribute_value = self.env.ref('bms_epc.bms_epc_record').golden_number_fee_id
        golden_attribute_value_ids = golden_attribute_value.value_ids
        print(golden_attribute_value_ids)
        search_att_co_roi = self.env['product.attribute.value'].search(
            [('name', '=', golden_number_fee), ('attribute_id', '=', golden_attribute_value.id)])
        print(search_att_co_roi)
        if len(search_att_co_roi) > 0:
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            print(list_name)
            if golden_attribute_value.name not in list_name:
                product.write({'attribute_line_ids': [(0, 0,
                                                       {'attribute_id': golden_attribute_value.id,
                                                        'value_ids': [(4, search_att_co_roi.id)]}
                                                       )]})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', golden_attribute_value.id], ['name', '=', golden_number_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(golden_number_fee)
            else:
                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == golden_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [
                                                                   (4, search_att_co_roi.id)]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', golden_attribute_value.id], ['name', '=', golden_number_fee], ['product_tmpl_id', '=', product_id]])
                        product_template_attribute_value_id.price_extra = int(golden_number_fee)
                        break
        else:
            print("dang khong co golden number fee co gia tri bang gia tri cu")
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            if golden_attribute_value.name not in list_name:
                new_attribute_id = self.env['product.attribute.value'].create(
                    {'name': golden_number_fee, 'attribute_id': golden_attribute_value.id})
                product.write({'attribute_line_ids': ([(0, 0, {'attribute_id': golden_attribute_value.id,
                                                               'value_ids': [(4, new_attribute_id.id)]
                                                               })])})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', golden_attribute_value.id], ['name', '=', golden_number_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(
                    golden_number_fee)
            if golden_attribute_value.name in list_name:
                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == golden_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [(0, 0, {
                                                                   'name': golden_number_fee,
                                                                   'attribute_id': golden_attribute_value.id})]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', golden_attribute_value.id], ['name', '=', golden_number_fee]])
                        product_template_attribute_value_id.price_extra = int(golden_number_fee)
        print("hoan thanh xong cong viec golden_number_fee")

    def check_setup_fee(self, product_id, setup_fee):

        product = self.env['product.template'].browse(product_id)
        setup_fee_attribute_value = self.env.ref(
            'bms_epc.bms_epc_record').setup_fee_id
        setup_fee_attribute_value_ids = setup_fee_attribute_value.value_ids
        search_att_co_roi = self.env['product.attribute.value'].search(
            [('name', '=', setup_fee), ('attribute_id', '=', setup_fee_attribute_value.id)])

        if len(search_att_co_roi) > 0:
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            if setup_fee_attribute_value.name not in list_name:
                product.write({'attribute_line_ids': [(0, 0,
                                                       {'attribute_id': setup_fee_attribute_value.id,
                                                        'value_ids': [(4, search_att_co_roi.id)]}
                                                       )]})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', setup_fee_attribute_value.id], ['name', '=', setup_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(setup_fee)
            else:

                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == setup_fee_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [
                                                                   (4, search_att_co_roi.id)]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', setup_fee_attribute_value.id], ['name', '=', setup_fee], ['product_tmpl_id', '=', product_id]])
                        product_template_attribute_value_id.price_extra = int(setup_fee)
                        break
        # if no create attribute
        else:
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            if setup_fee_attribute_value.name not in list_name:
                new_attribute_id = self.env['product.attribute.value'].create(
                    {'name': setup_fee, 'attribute_id': setup_fee_attribute_value.id})
                product.write({'attribute_line_ids': ([(0, 0, {'attribute_id': setup_fee_attribute_value.id,
                                                               'value_ids': [(4, new_attribute_id.id)]
                                                               })])})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', setup_fee_attribute_value.id], ['name', '=', setup_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(setup_fee)
            if setup_fee_attribute_value.name in list_name:
                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == setup_fee_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [(0, 0, {
                                                                   'name': setup_fee,
                                                                   'attribute_id': setup_fee_attribute_value.id})]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', setup_fee_attribute_value.id], ['name', '=', setup_fee], ['product_tmpl_id', '=', product_id]])
                        product_template_attribute_value_id.price_extra = int(setup_fee)
        print("hoan thanh xong cong viec setup_fee")

    def check_sub_fee(self, product_id, sub_fee):
        print("Thach check here")
        print(sub_fee)
        print(product_id)
        product = self.env['product.template'].browse(product_id)
        sub_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').sub_fee_id
        sub_fee_attribute_value_ids = sub_fee_attribute_value.value_ids
        founded = False
        search_att_co_roi = self.env['product.attribute.value'].search([('name', '=', sub_fee),
                                                                        ('attribute_id', '=', sub_fee_attribute_value.id)])
        if len(search_att_co_roi) > 0:
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            if sub_fee_attribute_value.name not in list_name:
                product.write({'attribute_line_ids': [(0, 0,
                                                       {'attribute_id': sub_fee_attribute_value.id,
                                                        'value_ids': [(4, search_att_co_roi.id)]}
                                                       )]})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', sub_fee_attribute_value.id], ['name', '=', sub_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(sub_fee)
            else:
                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == sub_fee_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [
                                                                   (4, search_att_co_roi.id)]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', sub_fee_attribute_value.id], ['name', '=', sub_fee], ['product_tmpl_id', '=', product_id]])
                        product_template_attribute_value_id.price_extra = int(sub_fee)

        else:
            list_name = []
            for name in product.attribute_line_ids:
                list_name.append(name.attribute_id.name)
            if sub_fee_attribute_value.name not in list_name:
                new_attribute_id = self.env['product.attribute.value'].create(
                    {'name': sub_fee, 'attribute_id': sub_fee_attribute_value.id})
                product.write({'attribute_line_ids': ([(0, 0, {'attribute_id': sub_fee_attribute_value.id,
                                                               'value_ids': [(4, new_attribute_id.id)]
                                                               })])})
                product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                    [['attribute_id', '=', sub_fee_attribute_value.id], ['name', '=', sub_fee], ['product_tmpl_id', '=', product_id]])
                product_template_attribute_value_id.price_extra = int(sub_fee)
            if sub_fee_attribute_value.name in list_name:
                for attribute_line_id in product.attribute_line_ids:
                    if attribute_line_id.attribute_id == sub_fee_attribute_value:
                        product.write({'attribute_line_ids': [(1, attribute_line_id.id,
                                                               {'value_ids': [(0, 0, {
                                                                   'name': sub_fee,
                                                                   'attribute_id': sub_fee_attribute_value.id})]}
                                                               )]})
                        product_template_attribute_value_id = self.env['product.template.attribute.value'].search(
                            [['attribute_id', '=', sub_fee_attribute_value.id], ['name', '=', sub_fee], ['product_tmpl_id', '=', product_id]])
                        product_template_attribute_value_id.price_extra = int(sub_fee)
        print("hoan thanh xong cong viec sub_fee")

    @api.model
    def update_did_number(self, did_number, golden_number_fee, setup_fee, sub_fee):
        if did_number.startswith("841900"):
            product_template_id = self.env.ref(
                'bms_epc.bms_epc_record').Product_1900
            self.check_golden_number_fee(
                product_template_id.id, golden_number_fee)
            self.check_setup_fee(product_template_id.id, setup_fee)
            self.check_sub_fee(product_template_id.id, sub_fee)
            sub_fee_attribute_value = self.env.ref(
                'bms_epc.bms_epc_record').sub_fee_id
            golden_attribute_value = self.env.ref(
                'bms_epc.bms_epc_record').golden_number_fee_id
            setup_fee_attribute_value = self.env.ref(
                'bms_epc.bms_epc_record').setup_fee_id
            search_attribute_value_golden = self.env['product.attribute.value'].search(
                [('name', '=', golden_number_fee), ('attribute_id', '=', golden_attribute_value.id)]).id
            search_attribute_value_setup = self.env['product.attribute.value'].search(
                [('name', '=', setup_fee), ('attribute_id', '=', setup_fee_attribute_value.id)]).id
            search_attribute_value_sub = self.env['product.attribute.value'].search(
                [('name', '=', sub_fee), ('attribute_id', '=', sub_fee_attribute_value.id)]).id
            list_id = [search_attribute_value_golden,
                       search_attribute_value_setup, search_attribute_value_sub]

            print(list_id)
            search_product = self.env['product.product'].search_read(
                [('attribute_value_ids', 'in', list_id), ('name', '=', product_template_id.name)], ['attribute_value_ids'])
            for product in search_product:
                for key, value in product.items():
                    if key == 'attribute_value_ids':
                        value.sort()
                        list_id.sort()
                        if value == list_id:
                            self.env['product.product'].search([('id', '=', int(product['id']))]).write(
                                {'isdn': did_number})
                            return product['id']

        if did_number.startswith("841800"):
            product_template_id = self.env.ref(
                'bms_epc.bms_epc_record').Product_1800
            self.check_golden_number_fee(
                product_template_id.id, golden_number_fee)
            self.check_setup_fee(product_template_id.id, setup_fee)
            self.check_sub_fee(product_template_id.id, sub_fee)
            sub_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').sub_fee_id
            golden_attribute_value = self.env.ref('bms_epc.bms_epc_record').golden_number_fee_id
            setup_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').setup_fee_id
            search_attribute_value_golden = self.env['product.attribute.value'].search(
                [('name', '=', golden_number_fee), ('attribute_id', '=', golden_attribute_value.id)]).id
            search_attribute_value_setup = self.env['product.attribute.value'].search(
                [('name', '=', setup_fee), ('attribute_id', '=', setup_fee_attribute_value.id)]).id
            search_attribute_value_sub = self.env['product.attribute.value'].search(
                [('name', '=', sub_fee), ('attribute_id', '=', sub_fee_attribute_value.id)]).id
            list_id = [search_attribute_value_golden,
                       search_attribute_value_setup, search_attribute_value_sub]

            search_product = self.env['product.product'].search_read(
                [('attribute_value_ids', 'in', list_id), ('name', '=', product_template_id.name)], ['attribute_value_ids'])
            print(search_product)
            for product in search_product:
                for key, value in product.items():
                    if key == 'attribute_value_ids':
                        value.sort()
                        list_id.sort()
                        if value == list_id:
                            print(product['id'])
                            self.env['product.product'].search(
                                [('id', '=', int(product['id']))]).write({'isdn': did_number})
                            return product['id']

        if did_number.startswith("842"):
            product_template_id = self.env.ref('bms_epc.bms_epc_record').number_cd
            self.check_golden_number_fee(product_template_id.id, golden_number_fee)
            self.check_setup_fee(product_template_id.id, setup_fee)
            self.check_sub_fee(product_template_id.id, sub_fee)
            sub_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').sub_fee_id
            golden_attribute_value = self.env.ref('bms_epc.bms_epc_record').golden_number_fee_id
            setup_fee_attribute_value = self.env.ref(
                'bms_epc.bms_epc_record').setup_fee_id
            search_attribute_value_golden = self.env['product.attribute.value'].search(
                [('name', '=', golden_number_fee), ('attribute_id', '=', golden_attribute_value.id)]).id
            search_attribute_value_setup = self.env['product.attribute.value'].search(
                [('name', '=', setup_fee), ('attribute_id', '=', setup_fee_attribute_value.id)]).id
            search_attribute_value_sub = self.env['product.attribute.value'].search(
                [('name', '=', sub_fee), ('attribute_id', '=', sub_fee_attribute_value.id)]).id
            list_id = [search_attribute_value_golden,
                       search_attribute_value_setup, search_attribute_value_sub]
            search_product = self.env['product.product'].search_read([('attribute_value_ids', 'in', list_id),
                                                                      ('name', '=',
                                                                       product_template_id.name)], ['attribute_value_ids'])
            print(search_product)
            for product in search_product:
                for key, value in product.items():
                    if key == 'attribute_value_ids':
                        value.sort()
                        list_id.sort()
                        if value == list_id:
                            self.env['product.product'].search([('id', '=', int(product['id']))]).write(
                                {'isdn': did_number})
                            return product['id']

        else:
            product_template_id = self.env.ref(
                'bms_epc.bms_epc_record').number_dd
            self.check_golden_number_fee(
                product_template_id.id, golden_number_fee)
            self.check_setup_fee(product_template_id.id, setup_fee)
            self.check_sub_fee(product_template_id.id, sub_fee)
            sub_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').sub_fee_id
            golden_attribute_value = self.env.ref('bms_epc.bms_epc_record').golden_number_fee_id
            setup_fee_attribute_value = self.env.ref('bms_epc.bms_epc_record').setup_fee_id
            search_attribute_value_golden = self.env['product.attribute.value'].search(
                [('name', '=', golden_number_fee), ('attribute_id', '=', golden_attribute_value.id)]).id
            search_attribute_value_setup = self.env['product.attribute.value'].search(
                [('name', '=', setup_fee), ('attribute_id', '=', setup_fee_attribute_value.id)]).id
            search_attribute_value_sub = self.env['product.attribute.value'].search(
                [('name', '=', sub_fee), ('attribute_id', '=', sub_fee_attribute_value.id)]).id
            list_id = [search_attribute_value_golden,
                       search_attribute_value_setup, search_attribute_value_sub]
            search_product = self.env['product.product'].search_read([('attribute_value_ids', 'in', list_id),
                                                                      ('name', '=',
                                                                       product_template_id.name)], ['attribute_value_ids'])
            for product in search_product:
                for key, value in product.items():
                    if key == 'attribute_value_ids':
                        value.sort()
                        list_id.sort()
                        if value == list_id:
                            self.env['product.product'].search([('id', '=', int(product['id']))]).write(
                                {'isdn': did_number})
                            return product['id']

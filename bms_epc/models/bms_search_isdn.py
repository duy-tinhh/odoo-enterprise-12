
from odoo import models, fields, api
import requests
import json
import random


class bmsEPC(models.Model):
    _name = 'bms.search.isdn'

    isdn = fields.Char("Số")
    golden_number_fee = fields.Char("Golden_number_fee")
    setup_fee = fields.Char("Setup_fee")
    sub_fee = fields.Char("sub_fee")
    check = fields.Boolean("Chọn số")
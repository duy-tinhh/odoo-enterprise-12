# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError


class wizard_with_step(models.Model):
    _name = 'bms.wizard'

    choose_number = fields.Char(string="Chọn số")
    product_ids = fields.Many2many('bms.search.isdn', string='Sản phẩm', store=True)

    @api.onchange('choose_number')
    def search_number(self):

        if self.choose_number != False:
            search_number = self.env['bms.epc'].find_did_number(self.choose_number)
            print(search_number)
            print(self.product_ids)
            number_ids = []
            if search_number:
                for product in search_number:
                    id = {
                        'golden_number_fee': product['golden_number_fee'],
                        'setup_fee': product['setup_fee'],
                        'sub_fee': product['sub_fee'],
                        'isdn': product['isdn'],
                    }
                    item = (0, 0, id)
                    number_ids.append(item)
            self.product_ids = number_ids

    def action_all(self):
        print(self.product_ids)
        print(self.env.context)
        sale_order = self.env['sale.order'].search([('id', '=', self.env.context['active_id'])])
        print(sale_order)
        for sale in self.product_ids:
            if sale.check == True:
                product = self.env['bms.epc'].update_did_number(sale.isdn, sale.golden_number_fee, sale.setup_fee,
                                                                sale.sub_fee)
                sale_order.write({'order_line': [(0, 0, {'product_id': int(product),
                                                         'DID': sale.isdn
                                                         })]})

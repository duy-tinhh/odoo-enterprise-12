# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError
import datetime



class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def choose_number(self):

        return {
            'name':'Chọn số',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'bms.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        currentDT = datetime.datetime.now()
        data_create = []
        for line in self.order_line:
            data_create.append((0, 0, {
                                    'product_id': line.product_id.id,
                                    'name': line.name,
                                    'date_planned': currentDT,
                                    'price_unit': line.price_unit,
                                    'product_qty': line.product_uom_qty,
                                    'taxes_id': [(4,line.tax_id.id)],
                                    'product_uom': 1,
                                    }))
        puchase = self.env['purchase.order'].create({'partner_id': 8,
                                                     'order_line': data_create})
        puchase.button_confirm()
        pickings = puchase.picking_ids.move_ids_without_package
        list_picking = []
        for product in pickings:
            product.write({'quantity_done': 1})
            list_picking.append(product)
        count = -1
        for line in self.order_line:
            count = count + 1
            for product in list_picking:
                if count == list_picking.index(product):
                    product.move_line_ids.write({'lot_name': line.DID})
        puchase.picking_ids.button_validate()
        count_3 = 0
        for line in self.order_line:
            count_3 = count_3 + 1
            count_2 = 0
            for sale_picking in self.picking_ids.move_ids_without_package:
                count_2 = count_2 + 1
                if count_2 == count_3:
                    stock_lot = self.env['stock.production.lot'].search([('name','=',line.DID)])
                    sale_picking.move_line_ids.lot_id = stock_lot
        return res



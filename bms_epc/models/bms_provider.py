# -*- coding: utf-8 -*-

from odoo import models, fields, api



class provider(models.Model):
    _name = 'bms.provider'

    name = fields.Char(string="Đầu số", required=True)
    provider_id = fields.Many2one(comodel_name="res.partner", string="Nhà mạng viễn thộng", required=True)


# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError


class Invoice(models.Model):
    _inherit = 'product.product'

    isdn = fields.Char("Số")
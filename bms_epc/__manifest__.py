# -*- coding: utf-8 -*-
{
    'name': "BMS_SIMSO",

    'summary': """
        Module DID Ecommerce for client search, book and payment in website
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "BMSTECH",
    'website': "http://www.bmstech.io",
    # support for show when filter App
    'installable': True,
    'application': True,
    'category': 'BMS Application',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'product'],

    'data': [
        'security/group.xml',
        'security/ir.model.access.csv',
        'data/data_bms_product_attribute.xml',
        'data/data_bms_epc_record.xml',

        'views/bms_epc_view.xml',
        'views/snippets.xml',
        'views/bms_sale_order.xml',
        'views/bms_wizard.xml',
        'views/bms_product_product.xml',
        'views/bms_search_isdn.xml',
        'views/bms_provider.xml',

        'views/menu_bms_epc.xml',
    ],

    'demo': [
    ],
}

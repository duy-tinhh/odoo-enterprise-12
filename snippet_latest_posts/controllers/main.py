import werkzeug
from odoo import http
from odoo.http import request


class snippet_latest_posts_controller(http.Controller):

    # @http.route(['/snippet_latest_posts/fetch'], type='json', auth='public', website=True)
    # def fetch_latest_posts(self, fields, domain, limit=None, order='published_date desc', context={}):
    #     return request.env['blog.post'].search_read(domain, fields, limit=limit, order=order, context=context)

    @http.route(['/snippet_latest_posts/render'], type='json', auth='public', website=True)
    def render_latest_posts(self, template, domain, limit=None, order='published_date desc'):
        # print(template)
        posts = request.env['blog.post'].search(
            domain, limit=limit, order=order)
        # print("XxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxX")
        # print(request.env.ref(template).render({'posts': posts}))
        # print(type(request.env.ref(template).render({'posts': posts})))
        # print("XxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxX")
        return request.env.ref(template).render({'posts': posts})

    @http.route(['/snippet_latest_posts/subscription/render'], type='json', auth='public', website=True)
    def render_latest_subscription_templates(self, limit=None, order='create_date desc'):
        subscription_templates = request.env['sale.subscription.template'].search([
                                                                                  ('active', '=', True)])
        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        print(subscription_templates)
        subscription_data = []
        product_ids = []
        option_product_ids = []
        accessory_product_ids = []

        for subscription_templates_id in subscription_templates:
            subscription_data.append(dict({
                'id': subscription_templates_id.id,
                'name': subscription_templates_id.name,
                'product_ids': subscription_templates_id.product_ids
            }))
            for pro_id in subscription_templates_id.product_ids:

                product_ids.append(pro_id)
        print(product_ids)
        for product_id in product_ids:
            for option_product_id in product_id.optional_product_ids:
                for accessory_product_id in option_product_id.accessory_product_ids:
                    accessory_product_ids.append(accessory_product_id)

        print(accessory_product_ids)
        newlist = [ii for n, ii in enumerate(
            accessory_product_ids) if ii not in accessory_product_ids[:n]]
        accessory_product_ids = newlist
        print(accessory_product_ids)

        print("______________________________________________________________")
        # return subscription_data
        return request.env.ref("snippet_latest_posts.navigation_sale_subscription").render({'subscription_templates': subscription_templates, 'accessory_product_ids': accessory_product_ids})

    @http.route(['/snippet_latest_posts/accessory/render'], type='json', auth='public', website=True)
    def render_acessory_templates(self, limit=None, order='create_date desc'):
        subscription_templates = request.env['sale.subscription.template'].search([
                                                                                  ('active', '=', True)])
        print("cheeeeeeeeeeeeeeeeessssssssssssseeeeeeeeeeeeeeee")
        print(subscription_templates)

        print("cheeeeeeeeeeeeeeeeessssssssssssseeeeeeeeeeeeeeee")
        # return subscription_data
        return request.env.ref("snippet_latest_posts.accessory_part").render()

odoo.define('snippet_latest_posts.order_product', function(require) {
  'use strict';

  var core = require('web.core');
  var ajax = require('web.ajax');
  var sAnimations = require('website.content.snippets.animation');
  var _t = core._t;
  var rpc = require('web.rpc');
  var utils = require('web.utils');
  var ttl = 24*60*60*365;//time to live of the cookie
  // utils.set_cookie('product_per_page', 12, ttl);
  utils.set_cookie('sale_subscription_template', 0, ttl);
  utils.set_cookie('ccall_user', 0, ttl);
  utils.set_cookie('product_template', [], ttl);
  utils.set_cookie('product_product', [], ttl);

  sAnimations.registry.OrderProduct = sAnimations.Class.extend({
    selector: '.s_latest_posts',
    read_events: {
      'click .minus-btn': '_onMinusButtonBMS',
      'click .plus-btn': '_onPlusButtonBMS',
      'click [id*="plus-account-number-"]': '_onPlusAccountNumberChange',
      'click [id*="minus-account-number-"]': '_onMinusAccountNumberChange',
      'click [id*="product-check-"]': '_onProductCheck',
    },

    /**
     * @private
     */
    _onProductCheck: function(ev) {
      var clicked_id = ev.currentTarget.id;
      console.log("&&&&&&&&&&&&&&&&&&&&&&");
      console.log(ev);
      clicked_id = ev.currentTarget.id.substring(14);

      console.log(clicked_id);
      var product_check_id = "#product-check-" + clicked_id;
      var checked_product = $(product_check_id).is(":checked");

      if (checked_product === true) {
        console.log($(product_check_id).is(":checked"));
        console.log("^^^^^^^^^^^^^^^^");
        var sm_id = $(product_check_id).attr("sm");
        var ten_sm_id = "#sm-id-" + sm_id;

        var check_value_id = "[type='account-number-" + sm_id + "']";
        var check_value_quantity = $(check_value_id).val();
        console.log(check_value_quantity);
        var group_product_name = $(ten_sm_id).text();
        var product_name_id = "#product-name-" + clicked_id;
        console.log(product_name_id);
        var product_name = $(product_name_id).text();
        console.log(product_name);


        // check what id is checked
        var noi_dung_gio_hang = '';
        var sm_checked_id = '[sm="' + sm_id + '"]:checked';
        $(sm_checked_id).each(function(index, val) {
          var product_line_id = $(val).attr("id").substring(14);
          var product_name_id = "#product-name-" + product_line_id;

          var product_name = $(product_name_id).text();
          var total_price_id = "#total-price-" + product_line_id;
          var total_price = $(total_price_id).text();

          console.log("iiiiiiiiiiiiiiiiiiiiiiiiiiiii");
          console.log(product_line_id);
          console.log(check_value_quantity);
          var add_to_cart = ajax.jsonRpc("/shop/cart/update_json", 'call', {
            'product_id': parseInt(product_line_id, 10),
            'set_qty': parseInt(check_value_quantity, 10),
          });
          console.log("iiiiiiiiiiiiiiiiiiiiiiiiiiiii");
          noi_dung_gio_hang = noi_dung_gio_hang + '<p><span class="item">' + product_name + '</span><span class="price">' + total_price + ' đ</span></p>';
        });

        var giohang_id = "giohang-" + sm_id;
        var cart_id = "#" + giohang_id;
        $(cart_id).remove();

        console.log("---------------------");
        $('<li class="register" id="' + giohang_id + '"><ul><li><strong>' + group_product_name + '</strong>' + '<p><span class="item">Số lượng máy nhánh</span><span class="price">' + check_value_quantity + ' người</span></p>' + noi_dung_gio_hang + '</li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));

      } else {
        console.log($(product_check_id).is(":checked"));
        console.log("^^^^^^^^^^^^^^^^");
        var sm_id = $(product_check_id).attr("sm");
        var ten_sm_id = "#sm-id-" + sm_id;

        var check_value_id = "[type='account-number-" + sm_id + "']";
        var check_value_quantity = $(check_value_id).val();
        console.log(check_value_quantity);
        var group_product_name = $(ten_sm_id).text();
        var product_name_id = "#product-name-" + clicked_id;
        console.log(product_name_id);
        var product_name = $(product_name_id).text();
        console.log(product_name);

        // check what id is checked
        var noi_dung_gio_hang = '';
        var sm_checked_id = '[sm="' + sm_id + '"]:checked';
        $(sm_checked_id).each(function(index, val) {
          var product_line_id = $(val).attr("id").substring(14);
          var product_name_id = "#product-name-" + product_line_id;
          var product_name = $(product_name_id).text();
          noi_dung_gio_hang = noi_dung_gio_hang + '<p><span class="item">' + product_name + '</span><span class="price">' + 30000 + ' đ</span></p>';
        });

        var giohang_id = "giohang-" + sm_id;
        var cart_id = "#" + giohang_id;
        $(cart_id).remove();

        console.log("---------------------");
        $('<li class="register" id="' + giohang_id + '"><ul><li><strong>' + group_product_name + '</strong>' + '<p><span class="item">Số lượng máy nhánh</span><span class="price">' + check_value_quantity + ' người</span></p>' + noi_dung_gio_hang + '</li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));
      };

    },

    /**
     * @private
     */
    _onPlusAccountNumberChange: function(ev) {
      var clicked_id = ev.currentTarget.id.substring(20);
      var choose_tap_id = "#nav-subscription-" + clicked_id;
      var quan = $(choose_tap_id).find(".num-quantity");
      var acc_num_id = "[type='account-number-" + clicked_id + "']";
      var num_input = $(acc_num_id).val();
      num_input++;
      $(acc_num_id).val(num_input);
      quan.text(num_input);

      var domain = [
        ['subscription_template_id', '=', parseInt(clicked_id)]
      ];
      var fields = ['id', 'name', 'optional_product_ids'];
      rpc.query({
        model: 'product.template',
        method: 'search_read',
        args: [domain, fields],
      }, {
        timeout: 3000,
        shadow: true,
      }).then(function(data) {
        data.forEach(function(each_product) {
          each_product['optional_product_ids'].forEach(function(option_product) {
            console.log("iiiiiiiiiiiiiiiii");
            rpc.query({
              model: 'bms.epc',
              method: 'find_product_price',
              args: [option_product, parseInt(num_input)],
            }, {
              timeout: 3000,
              shadow: true,
            }).then(function(discounted_price) {
              var list_price_id = "#list-price-" + option_product;
              var total_price_id = "#total-price-" + option_product;
              var total_price_row = discounted_price * parseInt(num_input);
              $(list_price_id).text(discounted_price);
              $(total_price_id).text(total_price_row);
              var sm_checked_id = '[sm="' + clicked_id + '"]:checked';
              var sm_checked = $(sm_checked_id);
              var noi_dung_gio_hang = '';
              $(sm_checked).each(function(index, val) {
                var product_line_id = $(val).attr("id").substring(14);
                var product_name_id = "#product-name-" + product_line_id;
                var product_name = $(product_name_id).text();
                var product_price_id = "#total-price-" + product_line_id;
                var product_price = $(product_price_id).text();
                noi_dung_gio_hang = noi_dung_gio_hang + '<p><span class="item">' + product_name + '</span><span class="price">' + product_price + ' đ</span></p>';
                // cap nhat vao gio hang
                var add_to_cart = ajax.jsonRpc("/shop/cart/update_json", 'call', {
                  'product_id': parseInt(product_line_id, 10),
                  'set_qty': parseInt(num_input, 10),
                });

              });

              console.log("vivivivivivivivivivivivivivivivvi");
              console.log(clicked_id);
              console.log(sm_checked);
              var giohang_id = "giohang-" + clicked_id;
              var cart_id = "#" + giohang_id;
              $(cart_id).remove();
              var ten_sm_id = "#sm-id-" + clicked_id;
              var group_product_name = $(ten_sm_id).text();

              console.log("---------------------");
              $('<li class="register" id="' + giohang_id + '"><ul><li><strong>' + group_product_name + '</strong>' + '<p><span class="item">Số lượng máy nhánh</span><span class="price">' + num_input + ' người</span></p>' + noi_dung_gio_hang + '</li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));

              console.log("vivivivivivivivivivivivivivivivvi");
            });
          });
        });
      });
    },

    /**
     * @private
     */
    _onMinusAccountNumberChange: function(ev) {
      var clicked_id = ev.currentTarget.id.substring(21);
      var choose_tap_id = "#nav-subscription-" + clicked_id;
      var quan = $(choose_tap_id).find(".num-quantity");
      var acc_num_id = "[type='account-number-" + clicked_id + "']"
      var num_input = $(acc_num_id).val();
      if (num_input > 0) {
        num_input--;
      };
      $(acc_num_id).val(num_input);
      quan.text(num_input);
    },

    /**
     * @private
     */
    _onMinusButtonBMS: function(ev) {
      // fix lai an tang giam so luong
      console.log("**********************************");
      var clicked_id = ev.currentTarget.id.substring(14);
      var input_name = 'input[type=product-quantity-' + clicked_id + ']';
      var current_quantity = parseInt($(input_name).val()) - 1;
      console.log(current_quantity);
      if (current_quantity > -1){
        console.log("pâppapapapoapapapap");
        $(input_name).val(current_quantity);
        var total_price_id = '#total-price-' + clicked_id;
        var list_price_id = '#list-price-' + clicked_id;
        var list_price = parseInt($(list_price_id).text().split(',').join(''));


        // update vao gio hang
        var add_to_cart = ajax.jsonRpc("/shop/cart/update_json", 'call', {
          'product_id': parseInt(clicked_id, 10),
          'set_qty': parseInt(current_quantity, 10),
        });
        var total_price = 0;
        var shopping_cart_total_price_id = '#shopping-cart-total-price-' + clicked_id;
        var product_name_id = '#product-name-' + clicked_id;
        var ten_san_pham = $(product_name_id).text();
        var shopping_cart_id = '#shopping-cart-name-' + clicked_id;
        var shopping_cart_quantity = '#shopping-cart-quantity-' + clicked_id;


        console.log("4 voi 4 la 8 - 8 voi 8 la 16");
        // cap nhat lai gia cua san pham
        rpc.query({
          model: 'bms.epc',
          method: 'find_product_product_price',
          args: [parseInt(clicked_id, 10), parseInt(current_quantity, 10)],
        }, {
          timeout: 3000,
          shadow: true,
        }).then(function(discounted_price) {
          console.log("1 voi 1 la 2 voi 2 la 4");
          total_price = current_quantity * discounted_price;
          $(list_price_id).text(discounted_price);
          $(total_price_id).text(total_price);
          $(shopping_cart_total_price_id).text(total_price);
          // add to shopping cart
          console.log(discounted_price);
          console.log("1 voi 1 la 2 voi 2 la 4");
          var noi_dung_gio_hang = '';
          noi_dung_gio_hang = noi_dung_gio_hang + '<p id="product-' + clicked_id + '"><span class="item">' + ten_san_pham + '</span><span class="price">' + total_price + ' đ</span></p>';
          console.log(noi_dung_gio_hang);
          var giohang_id = "giohang-accessory";
          var cart_id = "#" + giohang_id;
          var id_san_pham = "#product-" + clicked_id;
                console.log("4 voi 4 la 8 - 8 voi 8 la 16");

          if ($("#ctl00_ctl00").find(".accessory").length > 0) {
            if ($("#ctl00_ctl00").find(id_san_pham).length > 0) {
              if (current_quantity === 0){
                $(id_san_pham).replaceWith('');
              }else{
                $(id_san_pham).replaceWith(noi_dung_gio_hang);
              };
            } else {
              $(cart_id).find("p:last").after(noi_dung_gio_hang);
            };
          } else {
            $('<li class="accessory" id="' + giohang_id + '"><ul><li ><strong>Các phụ kiện đi kèm</strong>' + noi_dung_gio_hang + '</li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));
          }
        });

        // end cap nhat lai gia san pham
      };








      // fix lai an tang giam so luong
      //
      //
      // console.log("0000000000000000000000");
      // console.log(ev);
      // console.log("vivivivivivivivivivivivivivivivvi");
      // var clicked_id = ev.currentTarget.id.substring(14);
      // var input_name = 'input[type=product-quantity-' + clicked_id + ']';
      // var current_quantity = parseInt($(input_name).val());
      // if (current_quantity > 0) {
      //   current_quantity = current_quantity - 1;
      //   $(input_name).val(current_quantity);
      // };
      //
      //
      //
      // // add to shopping cart
      // var shopping_cart_id = '#shopping-cart-name-' + clicked_id;
      // var shopping_cart_quantity = '#shopping-cart-quantity-' + clicked_id;
      // if ($(shopping_cart_id).length > 0) {
      //   if (current_quantity > 0) {
      //     $(shopping_cart_quantity).text(current_quantity);
      //
      //   } else {
      //     // xoa dong hien tai neu gia tri ve 0
      //     $(shopping_cart_quantity).parent().remove();
      //   };
      // };
      console.log("abc xyzv vivivivivivivivivivivivivivivivvi");
    },

    /**
     * @private
     */
    _onPlusButtonBMS: function(ev) {
      console.log("**********************************");
      var clicked_id = ev.currentTarget.id.substring(13);
      var input_name = 'input[type=product-quantity-' + clicked_id + ']';
      var current_quantity = parseInt($(input_name).val()) + 1;
      $(input_name).val(current_quantity);
      var total_price_id = '#total-price-' + clicked_id;
      var list_price_id = '#list-price-' + clicked_id;
      var list_price = parseInt($(list_price_id).text().split(',').join(''));


      // update vao gio hang
      var add_to_cart = ajax.jsonRpc("/shop/cart/update_json", 'call', {
        'product_id': parseInt(clicked_id, 10),
        'set_qty': parseInt(current_quantity, 10),
      });
      var total_price = 0;
      var shopping_cart_total_price_id = '#shopping-cart-total-price-' + clicked_id;
      var product_name_id = '#product-name-' + clicked_id;
      var ten_san_pham = $(product_name_id).text();
      var shopping_cart_id = '#shopping-cart-name-' + clicked_id;
      var shopping_cart_quantity = '#shopping-cart-quantity-' + clicked_id;


      console.log("4 voi 4 la 8 - 8 voi 8 la 16");
      // cap nhat lai gia cua san pham
      rpc.query({
        model: 'bms.epc',
        method: 'find_product_product_price',
        args: [parseInt(clicked_id, 10), parseInt(current_quantity, 10)],
      }, {
        timeout: 3000,
        shadow: true,
      }).then(function(discounted_price) {
        console.log("1 voi 1 la 2 voi 2 la 4");
        total_price = current_quantity * discounted_price;
        $(list_price_id).text(discounted_price);
        $(total_price_id).text(total_price);
        $(shopping_cart_total_price_id).text(total_price);
        // add to shopping cart
        console.log(discounted_price);
        console.log("1 voi 1 la 2 voi 2 la 4");
        var noi_dung_gio_hang = '';
        noi_dung_gio_hang = noi_dung_gio_hang + '<p id="product-' + clicked_id + '"><span class="item">' + ten_san_pham + '</span><span class="price">' + total_price + ' đ</span></p>';
        console.log(noi_dung_gio_hang);
        var giohang_id = "giohang-accessory";
        var cart_id = "#" + giohang_id;
        var id_san_pham = "#product-" + clicked_id;
              console.log("4 voi 4 la 8 - 8 voi 8 la 16");
        if ($("#ctl00_ctl00").find(".accessory").length > 0) {
          if ($("#ctl00_ctl00").find(id_san_pham).length > 0) {
            $(id_san_pham).replaceWith(noi_dung_gio_hang);
          } else {
            $(cart_id).find("li").append(noi_dung_gio_hang);
          };
        } else {
          $('<li class="accessory" id="' + giohang_id + '"><ul><li ><strong>Các phụ kiện đi kèm</strong>' + noi_dung_gio_hang + '</li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));
        }
      });

      // end cap nhat lai gia san pham



    },
  });

  sAnimations.registry.ccall_find = sAnimations.Class.extend({
    selector: '.s_references',
    read_events: {
      'click #ccall_find': '_on_ccall_find_click',
      'click #view-cart-id': '_on_view_cart_click',
      'click #did_find': '_onDIDfind_click',
      'click [id*="add_did_to_cart-"]': '_onAdd_to_Cart_click',
      'click [id*="did-page-"]': '_onClickPageBMS',
      'click #did_view_more': '_onClickViewMore',
    },

    /**
     * @private
     */
    _onClickPageBMS: function(ev) {
      console.log("7777777777777777777777777");
      var clicked_id = ev.currentTarget.id.substring(10);
      var did_page_id = '#did-page-' + clicked_id;
      var page_class_id = '.page-index-' + clicked_id;
      $('#did_finded_number_block').find('.col-md-6').css("display", "none");
      $('#my_pagination').find('a').removeClass("active");
      $(did_page_id).addClass("active");
      // add class active
      var active_link = "#my_pagination>a:nth-child(" + clicked_id + ")";
      $(active_link).addClass("active");
      $(page_class_id).css("display", "block").fadeIn("slow");
    },

    /**
     * @private
     */
    _onClickViewMore: function(ev) {
      console.log("8888888888888888888888888888888888888888888888888");
      var clicked_id = ev.currentTarget.id;
      // get current val of did_view_more
      var current_page_val = $("#did_view_more").attr('val');
      var max_page_val = $("#did_view_more").attr('max-val');
      console.log(max_page_val);
      if (current_page_val === max_page_val) {
        var next_page_val = 1;
      } else {
        var next_page_val = parseInt(current_page_val, 10) + 1;
      };

      console.log(next_page_val);
      var page_class_id = '.page-index-' + next_page_val.toString();
      $('#did_finded_number_block').find('.col-md-6').css("display", "none");
      $(page_class_id).css("display", "block").fadeIn("slow");
      var current_page_val = $("#did_view_more").attr('val', next_page_val.toString());
    },

    /**
     * @private
     */
    _on_view_cart_click: function(ev) {
      window.location.href = "/shop/cart";
    },


    /**
     * @private
     */
    _onAdd_to_Cart_click: function(ev) {
      console.log("000000000000000000000000000000");
      console.log(ev.currentTarget.id);
      var clicked_id = ev.currentTarget.id.substring(16);
      console.log(clicked_id);
      console.log(utils.get_cookie("product_per_page"));
      utils.set_cookie('sale_subscription_template', 1, ttl);
      utils.set_cookie('ccall_user', 3, ttl);
      utils.set_cookie('product_template', [4,5], ttl);
      utils.set_cookie('product_product', [3,6], ttl);

      console.log(utils.get_cookie('sale_subscription_template'));
      console.log(utils.get_cookie('ccall_user'));
      console.log(utils.get_cookie('product_template'));
      var p_t = utils.get_cookie('product_template');
      var array = JSON.parse("[" + p_t + "]");
      console.log(typeof p_t);
      console.log(typeof array);
      console.log(array[0]);
      console.log(array[1]);
      console.log(utils.get_cookie('product_product'));


      // add to cart

      var gnf_id = '#gnf-' + clicked_id;
      var gnf = $(gnf_id).text();
      var setup_fee_id = '#setup-fee-' + clicked_id;
      var setup_fee = $(setup_fee_id).text();
      var sub_fee_id = '#sub-fee-' + clicked_id;
      var sub_fee = $(sub_fee_id).text();
      var did_number_id = '#did_number-' + clicked_id;

      $(did_number_id).remove();
      $('<li id="did_number-' + clicked_id + '"><ul><li><strong>' + clicked_id + '</strong><p><span class="item">Golden number fee</span><span class="price">' + gnf + 'đ</span></p><p><span class="item">Setup fee</span><span class="price">' + setup_fee + 'đ</span></p><p><span class="item">Sub fee</span><span class="price">' + sub_fee + 'đ</span></p></li></ul><a class="remove" href="#"></a></li>').insertBefore($("#ctl00_ctl00 li:last"));

      rpc.query({
        model: 'bms.epc',
        method: 'update_did_number',
        args: [clicked_id, gnf, setup_fee, sub_fee],
      }).then(function(data) {
        console.log(data);
        var product_id = data;
        var add_to_cart = ajax.jsonRpc("/shop/cart/update_json", 'call', {
          'product_id': parseInt(data, 10),
          'set_qty': 1,
        });
      });

      console.log("000000000000000000000000000000");
    },

    /**
     * @private
     */
    _onDIDfind_click: function() {
      var check_selected_id = $("#network :selected").text();
      var val_selected = $("#network :selected").val();
      console.log("11111111111111111111111");
      console.log(val_selected);
      console.log(utils.get_cookie("product_per_page"));

      console.log("11111111111111111111111");
      if (val_selected === "0") {
        var chuoi_tim_kiem = $("#sim").val();
      } else {
        var chuoi_tim_kiem = val_selected + '.*' + $("#sim").val();
      };
      console.log(chuoi_tim_kiem);
      rpc.query({
        model: 'bms.epc',
        method: 'find_did_number',
        args: [chuoi_tim_kiem],
      }).then(function(data) {
        $('#did_finded_number_block').find('.col-md-6').remove();
        $('#bang_so_dep').find('td').parents('tr').remove();
        var $table = $("chuoi_tim_kiem");
        var length_find_did = data.length;
        var number_of_page = 0;
        if (length_find_did % 8 == 0) {
          number_of_page = Math.round(length_find_did / 8);
        } else {
          number_of_page = Math.round((length_find_did - length_find_did % 8) / 8) + 1;
        };
        if (number_of_page === 0) {
          $('#did_finded_number_block').html("<div style='text-align: center; width: 100%'><h4>Không tìm thấy số nào trong danh sách</h4></div>");
          $('#my_pagination').css("display", "none");
        } else {
          var index_page = 1;
          $('#my_pagination').css("display", "block");
          $('#did_finded_number_block').html("");
          $('#did_view_more').attr('val', '1');
          $('#did_view_more').attr('max-val', number_of_page);
          index_page = 1;
          var index_num = 1;
          data.forEach(function(so_tim_duoc) {
            var anhlogo = '';
            console.log("x2123123123123123123");
            console.log(so_tim_duoc.telco);
            console.log("x2123123123123123123");
            if (index_page > 1) {
              if (so_tim_duoc.telco === "Indochina Telecom") {
                anhlogo = '<img src="/bms_epc/static/src/img/itel.png" width="60px" height="60px"/>';
              } else {
                anhlogo = 'LOGO';
              };
              var sodep = '<div class="col-md-6 page-index-' + index_page + '" style="display: none;"><div class="so_dep_tim_thay" id="did-' + so_tim_duoc.isdn + '"><div class="col-xs-3 col-sm-2 col-md-2 sm20"><div style="margin-left: -15px;">' + anhlogo + '</div></div><div class="col-xs-6 col-sm-7 col-md-7 sm60"><h5 class="sodep">';
            } else {
              var sodep = '<div class="col-md-6 page-index-' + index_page + '" style="display:block;"><div class="so_dep_tim_thay" id="did-' + so_tim_duoc.isdn + '"><div class="col-xs-3 col-sm-2 col-md-2 sm20"><div style="margin-left: -15px;">' + anhlogo + '</div></div><div class="col-xs-6 col-sm-7 col-md-7 sm60"><h5 class="sodep">';
            };
            var total_did_price = so_tim_duoc.golden_number_fee + so_tim_duoc.setup_fee + so_tim_duoc.sub_fee;
            sodep = sodep + '<strong>' + so_tim_duoc.isdn + '</strong><br/></h5>';

            sodep = sodep + '<span id="gnf-' + so_tim_duoc.isdn + '" style="display:none;">' + so_tim_duoc.golden_number_fee + '</span>';
            sodep = sodep + '<span id="setup-fee-' + so_tim_duoc.isdn + '" style="display:none;">' + so_tim_duoc.setup_fee + '</span>';
            sodep = sodep + '<span id="sub-fee-' + so_tim_duoc.isdn + '" style="display:none;">' + so_tim_duoc.sub_fee + '</span>';
            sodep = sodep + total_did_price + '</div><div class="col-xs-3 col-sm-3 col-md-3 sm20"><a id="add_did_to_cart-' + so_tim_duoc.isdn + '">Add to cart</a></div></div></div>';
            $('#did_finded_number_block').append(sodep);
            index_num = index_num + 1;
            if (index_num % 8 == 1) {
              index_page = index_page + 1;
            };
          });
        };
      });
    },
  });

});

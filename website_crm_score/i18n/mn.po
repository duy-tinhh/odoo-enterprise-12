# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_crm_score
# 
# Translators:
# Khishigbat Ganbold <khishigbat@asterisk-tech.mn>, 2018
# Martin Trigaux, 2018
# Baskhuu Lodoikhuu <baskhuujacara@gmail.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-21 14:07+0000\n"
"PO-Revision-Date: 2018-08-24 11:49+0000\n"
"Last-Translator: Baskhuu Lodoikhuu <baskhuujacara@gmail.com>, 2019\n"
"Language-Team: Mongolian (https://www.transifex.com/odoo/teams/41243/mn/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: mn\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__pageviews_count
msgid "# Page Views"
msgstr "# Хуудсыг үзсэн"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_needaction
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_needaction
msgid "Action Needed"
msgstr "Үйлдэл Шаардлагатай"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__running
msgid "Active"
msgstr "Идэвхитэй"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_ir_ui_view__track
#: model:ir.model.fields,help:website_crm_score.field_website_page__track
msgid "Allow to specify for one page of the website to be trackable or not"
msgstr "Вэбсайтын нэг хуудсыг хөтлөх эсэхийг тодорхойлох бол зөвшөөрнө үү"

#. module: website_crm_score
#: selection:website.crm.score,rule_type:0
msgid "Archive"
msgstr "Архив"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
msgid "Assignation"
msgstr "Оноолт"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__leads_count
msgid "Assigned Leads"
msgstr "Оноогдсон сэжмүүд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__assigned_leads_count
msgid "Assigned Leads Count"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__leads_count
msgid "Assigned Leads this last month"
msgstr "Энэ сүүлийн сард оноогдсон сэжмүүд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_attachment_count
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_attachment_count
msgid "Attachment Count"
msgstr "Хавсралтын тоо"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__assign_date
msgid "Auto Assign Date"
msgstr ""

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
#: model_terms:ir.ui.view,arch_db:website_crm_score.team_user_kanban
msgid "Avatar"
msgstr "Зураг"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__capacity
msgid "Capacity"
msgstr "Хүчин чадал"

#. module: website_crm_score
#: model_terms:ir.actions.act_window,help:website_crm_score.team_user_action
msgid "Create a new salesman"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__create_uid
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__create_uid
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__create_uid
msgid "Created by"
msgstr "Үүсгэгч"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__create_date
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__create_date
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__create_date
msgid "Created on"
msgstr "Үүсгэсэн"

#. module: website_crm_score
#: model:ir.actions.server,name:website_crm_score.action_score_now
msgid "Crm Score: Apply marked scores"
msgstr ""

#. module: website_crm_score
#: model:ir.actions.server,name:website_crm_score.ir_cron_lead_assign_ir_actions_server
#: model:ir.cron,cron_name:website_crm_score.ir_cron_lead_assign
#: model:ir.cron,name:website_crm_score.ir_cron_lead_assign
msgid "Crm Score: lead assignation"
msgstr ""

#. module: website_crm_score
#: model:ir.actions.server,name:website_crm_score.ir_cron_lead_scoring_ir_actions_server
#: model:ir.cron,cron_name:website_crm_score.ir_cron_lead_scoring
#: model:ir.cron,name:website_crm_score.ir_cron_lead_scoring
msgid "Crm Score: lead scoring "
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__last_run
msgid "Date from the last scoring on all leads."
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_crm_lead__assign_date
msgid ""
"Date when the lead has been assigned via the auto-assignation mechanism"
msgstr "Сэжмийг авто-оноолтын механизмаар оноосон огноо"

#. module: website_crm_score
#: model_terms:ir.actions.act_window,help:website_crm_score.team_action
msgid "Define a new sales channel"
msgstr ""

#. module: website_crm_score
#: selection:website.crm.score,rule_type:0
msgid "Delete"
msgstr "Устгах"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__display_name
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__display_name
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__display_name
msgid "Display Name"
msgstr "Дэлгэцийн Нэр"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__score_team_domain
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__team_user_domain
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__domain
msgid "Domain"
msgstr "Домэйн"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__event_based
msgid "Event-based rule"
msgstr "Арга хэмжээ дээр суурилсан дүрэм"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_follower_ids
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_follower_ids
msgid "Followers"
msgstr "Дагагчид"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_channel_ids
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_channel_ids
msgid "Followers (Channels)"
msgstr "Дагагчид (Сувагууд)"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_partner_ids
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_partner_ids
msgid "Followers (Partners)"
msgstr "Дагагчид (Харилцагчид)"

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_ir_http
msgid "HTTP Routing"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__id
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__id
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__id
msgid "ID"
msgstr "ID"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_unread
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_unread
msgid "If checked new messages require your attention."
msgstr ""
"Хэрэв тэмдэглэгдсэн бол таныг шинэ зурвасуудад анхаарал хандуулахыг "
"шаардана."

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_needaction
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_needaction
msgid "If checked, new messages require your attention."
msgstr "Хэрэв сонгосон бол, шинэ зурвасуудад анхаарал хандуулахыг шаардана."

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_has_error
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_has_error
msgid "If checked, some messages have a delivery error."
msgstr "Үүнийг сонговол алдаа үүсэх үед зурвасууд ирнэ."

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_is_follower
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_is_follower
msgid "Is Follower"
msgstr "Дагагч эсэх"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__lang_id
msgid "Language"
msgstr "Хэл"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_crm_lead__lang_id
msgid "Language from the website when lead has been created"
msgstr "Сэжмийг үүсгэсэн үеийн вэбсайтын хэл"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user____last_update
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview____last_update
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score____last_update
msgid "Last Modified on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__write_uid
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__write_uid
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__write_uid
msgid "Last Updated by"
msgstr "Сүүлийн засвар хийсэн"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__write_date
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__write_date
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__write_date
msgid "Last Updated on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__last_run
msgid "Last run"
msgstr "Сүүлд ажилласан"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__lead_id
msgid "Lead"
msgstr "Удирдах"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.website_crm_score_pageviews
#: model:ir.ui.menu,name:website_crm_score.pageview_menu
msgid "Lead Page Views"
msgstr ""

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_crm_lead
msgid "Lead/Opportunity"
msgstr "Сэжим/Боломж"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.score_leads
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_score_form
msgid "Leads"
msgstr "Сэжим"

#. module: website_crm_score
#: model:ir.ui.menu,name:website_crm_score.team_user
msgid "Leads Assignation"
msgstr "Сэжмийн Оноолт"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__leads_count
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__leads_count
msgid "Leads Count"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__maximum_user_leads
msgid "Leads Per Month"
msgstr "Сарын сэжмүүд"

#. module: website_crm_score
#: model_terms:ir.actions.act_window,help:website_crm_score.team_user_action
msgid ""
"Link users to salesteam, set a per 30 days lead capacity for each of them "
"and set filters to auto assign your leads."
msgstr ""
"Сэжмүүдийг автоматаар оноохын тулд хэрэглэгчдийг борлуулалтын багт холбож, "
"тус бүрт нь 30 өдөр тутмын сэжмийн хүчин чадлыг тохируулж, шүүлтүүр "
"тохируулаарай."

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_crm_lead__score_pageview_ids
msgid "List of (tracked) pages seen by the owner of this lead"
msgstr "Энэ сэжмийн эзний үзсэн (хөтлөсөн) хуудсын жагсаалт"

#. module: website_crm_score
#. openerp-web
#: code:addons/website_crm_score/static/src/xml/track_page.xml:9
#, python-format
msgid "Log visits on leads"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_main_attachment_id
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_main_attachment_id
msgid "Main Attachment"
msgstr "Үндсэн хавсралт"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_team_user_form
msgid "Maximum Leads / 30 days"
msgstr "Хамгийн их Сэжмүүд / 30 өдөр"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_has_error
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_has_error
msgid "Message Delivery error"
msgstr "Алдаа үүссэн талаарх мессеж"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_ids
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_ids
msgid "Messages"
msgstr "Зурвасууд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__min_for_assign
msgid "Minimum score"
msgstr "Хамгийн бага оноо"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_crm_team__min_for_assign
msgid "Minimum score to be automatically assign (>=)"
msgstr "Автоматаар оноох онооны доод хэмжээ (>=)"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__name
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__name
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_score_form
msgid "Name"
msgstr "Нэр"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_needaction_counter
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_needaction_counter
msgid "Number of Actions"
msgstr "Үйлдлийн тоо"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_has_error_counter
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_has_error_counter
msgid "Number of error"
msgstr "Алдааны тоо"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_needaction_counter
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Үйлдэл шаардлагатай зурвасын тоо"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_has_error_counter
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Алдааны мэдэгдэл бүхий зурвасын тоо"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__message_unread_counter
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__message_unread_counter
msgid "Number of unread messages"
msgstr "Уншаагүй зурвасын тоо"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.crm_score_views_action_table
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_case_graph_view_leads_sales
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_case_table_view_leads_sales
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
msgid "Opportunities"
msgstr "Боломжууд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__score_pageview_ids
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_score_pageview_form
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_score_pageview_graph
msgid "Page Views"
msgstr "Хуудсыг үзэх тоо"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.lead_score_form
msgid "Page views"
msgstr "Хуудсыг үзсэн тоо"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__percentage_leads
msgid "Percentage leads"
msgstr "Хувь сэжмүүд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__phone
msgid "Phone"
msgstr "Утас"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__ratio
msgid "Ratio"
msgstr "Харьцаа"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__rule_type
msgid "Rule Type"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__running
msgid "Running"
msgstr "Ажиллаж буй"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__team_id
msgid "SaleTeam"
msgstr "Борлуулалтын Баг"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__user_id
msgid "Saleman"
msgstr "Борлуулагч"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.team_action
#: model:ir.model,name:website_crm_score.model_crm_team
msgid "Sales Channels"
msgstr "Борлуулалтын Сувгууд"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.team_user_action
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_team_user_form
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_team_user_tree
msgid "Sales Men"
msgstr "Борлуулагч нар"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_res_users__team_user_ids
msgid "Sales Records"
msgstr "Борлуулалтын бичлэгүүд"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_res_users__sale_team_id
msgid ""
"Sales Team the user is member of. Used to compute the members of a Sales "
"Team through the inverse one2many"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_team__team_user_ids
msgid "Salesman"
msgstr "Борлуулагч"

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_team_user
msgid "Salesperson (Team Member)"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__score
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_lead_view_dashboard
msgid "Score"
msgstr "Оноо"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.score_action
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_score_form
#: model_terms:ir.ui.view,arch_db:website_crm_score.view_crm_score_tree
msgid "Scores"
msgstr "Оноо"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.lead_score_form
#: model_terms:ir.ui.view,arch_db:website_crm_score.score_opp_form_view
#: selection:website.crm.score,rule_type:0
msgid "Scoring"
msgstr "Оноо өгөх"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_crm_lead__score_ids
#: model:ir.ui.menu,name:website_crm_score.scores_menu
msgid "Scoring Rules"
msgstr "Оноо өгөх Дүрэм"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__rule_type
msgid ""
"Scoring will add a score of `value` for this lead.\n"
"Archive will set active = False on the lead (archived)\n"
"Delete will delete definitively the lead\n"
"\n"
"Actions are done in sql and bypass the access rights and orm mechanism (create `score`, write `active`, unlink `crm_lead`)"
msgstr ""
"Оноо өгөх нь энэ сэжимд `утга` оноог нэмнэ.\n"
"Архивлах нь (архивласан) Сэжим дээр идэвхитэйг = Худал болгоно\n"
"Устгах нь сэжмийг буцалтгүйгээр устгана\n"
"\n"
"Үйлдлүүдийг sql дээр хийдэг ба хандалтын эрх болон orm механизмыг тойрдог (create `score`, write `active`, unlink `crm_lead`)"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.crm_score_pageview_filter
msgid "Search PageViews"
msgstr "Хуудсын харагдцуудыг хайх"

#. module: website_crm_score
#: model:ir.ui.menu,name:website_crm_score.team
msgid "Teams Assignation"
msgstr "Багийн Оноолт"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_ir_ui_view__track
#: model:ir.model.fields,field_description:website_crm_score.field_website_page__track
msgid "Track"
msgstr "Зам"

#. module: website_crm_score
#: model:ir.actions.act_window,name:website_crm_score.crm_score_views_action_graph
msgid "Unassigned leads"
msgstr "Оноогдоогүй сэжмүүд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_unread
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_unread
msgid "Unread Messages"
msgstr "Уншаагүй Зурвасууд"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__message_unread_counter
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__message_unread_counter
msgid "Unread Messages Counter"
msgstr "Уншаагүй зурвасын тоолуур"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__url
msgid "Url"
msgstr "Url"

#. module: website_crm_score
#: model_terms:ir.actions.act_window,help:website_crm_score.team_action
msgid ""
"Use sales channels to organize your sales departments.\n"
"                    Each channel will work with a separate pipeline."
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__user_id
msgid "User"
msgstr "Хэрэглэгч"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_res_users__sale_team_id
msgid "User Sales Channel"
msgstr ""

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_res_users
msgid "Users"
msgstr "Хэрэглэгчид"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__value
msgid "Value"
msgstr "Утга"

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_ir_ui_view
msgid "View"
msgstr "Харагдац"

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_pageview__view_date
msgid "Viewing Date"
msgstr "Үзсэн огноо"

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_website_crm_pageview
msgid "Website CRM Page View"
msgstr ""

#. module: website_crm_score
#: model:ir.model,name:website_crm_score.model_website_crm_score
msgid "Website CRM Score"
msgstr ""

#. module: website_crm_score
#: model:ir.model.fields,field_description:website_crm_score.field_team_user__website_message_ids
#: model:ir.model.fields,field_description:website_crm_score.field_website_crm_score__website_message_ids
msgid "Website Messages"
msgstr "Вебсайтын зурвасууд"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.website_crm_pageview_pivot
msgid "Website Pages"
msgstr "Вебсайт Хуудсууд"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_team_user__website_message_ids
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__website_message_ids
msgid "Website communication history"
msgstr "Вебсайтын харилцааны түүх"

#. module: website_crm_score
#: model:ir.model.fields,help:website_crm_score.field_website_crm_score__event_based
msgid ""
"When checked, the rule will be re-evaluated every time, even for leads that "
"have already been checked previously. This option incurs a large performance"
" penalty, so it should be checked only for rules that depend on dynamic "
"events"
msgstr ""
"Үүнийг чагталвал дүрмийг аль хэдийн шалгасан сэжим дээр ч гэсэн үргэлж дахин"
" үнэлнэ. Энэ сонголт нь том гүйцэтгэлийн торгуулийг гаргадаг тул үүнийг "
"зөвхөн динамик арга хэмжээнээс хамаарсан дүрэм дээр шалгах ёстой"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
#: model_terms:ir.ui.view,arch_db:website_crm_score.team_user_kanban
msgid "fa-check"
msgstr "fa-check"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
#: model_terms:ir.ui.view,arch_db:website_crm_score.team_user_kanban
msgid "fa-times"
msgstr "fa-times"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.website_crm_score_view_kanban
msgid "leads"
msgstr "сэжимүүд"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
#: model_terms:ir.ui.view,arch_db:website_crm_score.team_user_kanban
msgid "o_assignation_button_active"
msgstr "o_assignation_button_active"

#. module: website_crm_score
#: model_terms:ir.ui.view,arch_db:website_crm_score.sales_team_form_view_assign
#: model_terms:ir.ui.view,arch_db:website_crm_score.team_user_kanban
msgid "o_assignation_button_inactive"
msgstr "o_assignation_button_inactive"
